#!/bin/sh
IMAGENAME=$1

if [ -z "$IMAGENAME" ]; then
    IMAGENAME=$(sed -r -e 's/[[:space:]]+/-/g' /etc/slackware-version).tar.gz
fi

rm -f /mnt/$IMAGENAME

cd /
/usr/bin/tar \
    --one-file-system\
    --anchored\
    --exclude-caches\
    --warning=no-xdev\
    --warning=no-cachedir\
    -c\
    -z\
    -v\
    --exclude 'README' \
    --exclude var/lib/slackimg\
    --exclude 'mnt/*' \
    --exclude 'proc/*' \
    --exclude 'sys/*' \
    --exclude 'var/log/removed_packages/*' \
    --exclude 'var/log/removed_scripts/*' \
    -f /mnt/$IMAGENAME *

