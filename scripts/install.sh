#!/bin/sh

PKGLIST=/var/lib/slackpkg/pkglist

exclude_packages() {
    grep -v '^slackpkg-'
}

install_series() {
    echo "Installing series $1"
    pkglist=$(sed -n -r -e 's/^slackware64 (([^ ]+) ){4}([^ ]+) .\/slackware64\/('"$1"') ([^ ]+)[[:space:]]*$/\3/p' $PKGLIST | exclude_packages)
    if [ -n "$pkglist" ]; then	
	echo $pkglist | xargs slackpkg install
    fi
}

for s in $*
do
    install_series $s
done
