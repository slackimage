# #########################
# slackpkg setup

MIRROR=$1

. /etc/os-release
if [ "$VERSION_CODENAME" = "current" ]; then
    echo "Installing coreutils 8.25 to work around docker bug"
    cd /tmp
    wget http://pirx.gnu.org.ua/slackware/slackware64-14.2/slackware64/a/coreutils-8.25-x86_64-2.txz
    installpkg ./coreutils-8.25-x86_64-2.txz
    removepkg $(find /var/log/packages/ -name 'coreutils*' -not -name coreutils-8.25-x86_64-2)
    cd /usr/bin
    ln -sf mktemp-gnu mktemp
    cd /
    : ${MIRROR:=http://slackware.osuosl.org/slackware64-current/}
else
    : ${MIRROR:=https://pirx.gnu.org.ua/slackware/slackware64-$VERSION/}
fi

case $MIRROR in
    */) ;;
    *)  MIRROR=${MIRROR}/
esac

SLACKPKG_CONF=/etc/slackpkg/slackpkg.conf
MIRRORS_FILE=/etc/slackpkg/mirrors
PKGLIST=/var/lib/slackpkg/pkglist

# #########################
# Configure slackpkg
cat >> $SLACKPKG_CONF <<EOF
WGETFLAGS="--passive-ftp -nv"
DELALL=on
CHECKMD5=on
CHECKGPG=on
CHECKSIZE=off
PRIORITY=( patches %PKGMAIN extra pasture testing )
POSTINST=off
DIALOG=off
DOWNLOAD_ALL=on
SPINNING=off
EOF

echo $MIRROR > $MIRRORS_FILE

# #########################
# Initialize slackpkg
yes|slackpkg update

# #########################
# Reinstall existing packages
# Rationale: the ones installed by the image can be trimmed down
# to save space.  Apparently that's the case with vbatts/slackware images.
find /var/log/packages -maxdepth 1 -type f -printf '%f\n' |\
    grep -v ^slackpkg |\
    while read name ; do slackpkg reinstall $name; done
