ARG VERSION
FROM vbatts/slackware:${VERSION}
ARG MIRROR
RUN wget --no-check-certificate -O /etc/ssl/certs/DST_Root_CA_X3.pem https://letsencrypt.org/certs/trustid-x3-root.pem.txt &&\
 cd /etc/ssl/certs &&\
 ln -sf DST_Root_CA_X3.pem `openssl x509 -noout -hash -in DST_Root_CA_X3.pem`.0

ADD scripts/ /var/lib/slackimg
RUN sh /var/lib/slackimg/slackpkg-init.sh ${MIRROR}
ARG SERIES="a ap d e f k l n t tcl"
RUN sh /var/lib/slackimg/install.sh ${SERIES}
VOLUME ["/mnt"]
ENTRYPOINT ["/bin/sh", "/var/lib/slackimg/dump.sh" ]



