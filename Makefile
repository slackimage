ifeq (,$(wildcard .env))
.env:
	@echo "Creating default .env file"
	@{ \
              echo "## Version of Slackware to build the image for"; \
	      echo "VERSION=current"; \
              echo "## Include packages from these series:"; \
              echo "#SERIES=a ap d e f k l n t tcl"; \
              echo "## Output directory:"; \
              echo "OUTPUT=$$(pwd)/out"; \
              echo "## Name of the image tarball:"; \
              echo "IMAGE=slackware-current-64.tar.gz"; \
              echo "## Additional tag for the produced docker image.  The"; \
              echo "## image tag (in the docker sense) will be created by"; \
              echo '## concatenating $$VERSION and $$TAG.  If not empty, it'; \
              echo '## is recommended to start the $$TAG value with underscore.'; \
              echo "TAG="; \
              echo "## URL of the Slackware mirror site (selected automatically)"; \
              echo "#MIRROR="; \
        } > .env
	@echo "Please edit the .env file and restart make"
	@exit 1
else
  include .env
endif

TARGET = $(OUTPUT)/$(IMAGE)

$(TARGET): .env Makefile
	docker-compose up --build $(OPTS)

